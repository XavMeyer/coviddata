const fs = require('fs');
const readline = require('readline');

const INPUT_FILE_PATH = './re_data/dailyRe-Data-master/CHE-estimates.csv';
const OUTPUT_DIR_PATH = './re_data/processed';

const readReCSV = async function() {
    const fileStream = fs.createReadStream(INPUT_FILE_PATH);
    const rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity
    });

    let headers = [];
    let entryList = [];

    let firstLine = true;
    for await (const line of rl) {

        if(firstLine) {
            headers = line.split(",");
            firstLine = false;
        } else {
            let values = line.split(",");
            let newEntry = {};
            console.assert(headers.length === values.length);
            for(let i=0; i < values.length; ++i) {
                newEntry[headers[i]] = values[i];
            }
            entryList.push(newEntry);
        }
    }
    return entryList;
}

const sortEntriesByDate = function(entries) {
    entries.sort( (a,b) => { return new Date(a.date) - new Date(b.date)} );
    return entries;
}

const writeJSONFiles = function(entries) {
    let regions = [];
    entries.forEach(entry => regions.push(entry.region));
    let uniqueRegions = new Set(regions);
    for(let region of uniqueRegions){
        let entriesByRegion = entries.filter(entry => entry.region === region);
        let sortedEntries = sortEntriesByDate(entriesByRegion);
        let stringifiedEntries = JSON.stringify(sortedEntries, null, 4);
        let filename = OUTPUT_DIR_PATH + '/CHE_' + region.replace(/ /g, "_") + ".json";
        fs.writeFile(filename, stringifiedEntries, (err) => {
            if (err) throw err;
            else console.log("Written : " + region);
        });
    }
}

const preProcessReData = function() {
    try {
        const promisedEntries = readReCSV();
        promisedEntries.then(entries => writeJSONFiles(entries));
    } catch (err) {
        console.log(err);
    }
}

module.exports = { preProcessReData };
