const {preProcessCasesData} = require("./preProcessCasesData");
const {preProcessReData} = require("./preProcessReData");

// Preprocess the incidence data
console.log('Preprocessing Re data');
preProcessReData();

// preprocess the cases data
console.log('Preprocessing Cases data');
preProcessCasesData();