const fs = require('fs');
const Loess = require('loess');

// Load the base covid DATA
const INPUT_DATA_DIR = '../last_data/data/'
const OUTPUT_DATA_DIR = './last_data/processed/'

const SWISS_CANTONS = require('../cantonsInfo.json');
const COVID_DATA_CASES = require(INPUT_DATA_DIR+'COVID19Cases_geoRegion.json');
const COVID_DATA_HOSPS = require(INPUT_DATA_DIR+'COVID19Hosp_geoRegion.json');
const COVID_DATA_DEATHS = require(INPUT_DATA_DIR+'COVID19Death_geoRegion.json');
const COVID_DATA_TEST = require(INPUT_DATA_DIR+'COVID19Test_geoRegion_PCR_Antigen.json');

const DICT_KEYS = ['cases', 'hosps', 'deaths', 'tests', 'positifs'];
const NB_OF_DAYS = [7, 14, 28];
const TO_KEEP_ATTRIBUTES = ['datum', 'pop', 'entries', 'fitted', 'y', 'yFitted', 'yPer100K', 'yFittedPer100K'];

const COVID_DATA_DICT = {
  'cases' : COVID_DATA_CASES,
  'hosps' : COVID_DATA_HOSPS,
  'deaths' : COVID_DATA_DEATHS,
  'tests' : COVID_DATA_TEST,
  'positifs' : COVID_DATA_TEST
}


const sortDataByDate = function(data) {
  let sortedData = data;
  sortedData.sort( (a,b) => { return new Date(a.datum) - new Date(b.datum)} );
  return sortedData;
}

const defineGeoRegions = function() {
  let setRegions = new Set(SWISS_CANTONS.map(entry => entry.Abrev));
  return Array.from(setRegions).sort();
}

const defineCasePerRegion = function(region = 'CH', data) {
  return data.filter(entry => entry.geoRegion === region);
}

const estimateFittedData = function(data) {

  let inputData = {
    x : Array.from({length:data.length},(v,k)=>k+1),
    y : data.map(entry => entry.entries)
  }

  let options = {
    span: 21./data.length, // 0 to inf, default 0.75
    band: 0, // 0 to 1, default 0
    degree: 2, // default 2
    normalize: false, // default true if degree > 1, false otherwise
    robust: false // default false
    //iterations: integer //default 4 if robust = true, 1 otherwise
  }

  let model = new Loess.default(inputData, options);
  let predictedData = model.predict();
  return predictedData;
}

const mergeTestsDatasets = function(region) {
  let regionData = defineCasePerRegion(region, COVID_DATA_DICT['tests']);
  let regionDataPCR = regionData.filter(entry => entry.nachweismethode === 'PCR');
  let regionDataAntigen = regionData.filter(entry => entry.nachweismethode === 'Antigen_Schnelltest');

  let sortedDataPCR = sortDataByDate(regionDataPCR);
  let sortedDataAntigen = sortDataByDate(regionDataAntigen);

  console.assert(sortedDataPCR.length === sortedDataAntigen.length)
  let preprocessedData = [];
  for(let i=0; i<sortedDataPCR.length; i++) {
    console.assert(sortedDataPCR[i].datum === sortedDataAntigen[i].datum);
    let newObj = {
      'datum' : sortedDataPCR[i].datum,
      'pop' : sortedDataPCR[i].pop,
      'entries' : 0 };
    if('entries' in sortedDataPCR[i]) newObj.entries += sortedDataPCR[i].entries;
    if('entries' in sortedDataAntigen[i]) newObj.entries += sortedDataAntigen[i].entries;
    preprocessedData.push(newObj);
  }

  return preprocessedData;
}

const defineDataForCombinedChart = function(
  region = 'CH',
  dataType = 'cases') {

  let preprocessedData;
  if(dataType === 'positifs') {
    let regionCaseData = defineCasePerRegion(region, COVID_DATA_DICT['cases']);
    let sortedCaseData = sortDataByDate(regionCaseData);

    let sortedTestData = mergeTestsDatasets(region);

    let firstDate = sortedCaseData[0].datum;
    while(sortedTestData[0].datum !== firstDate) {
      sortedTestData.shift();
    }

    while(sortedTestData.length > sortedCaseData.length) {
      sortedTestData.pop();
    }

    while(sortedCaseData.length > sortedTestData.length) {
      sortedCaseData.pop();
    }

    preprocessedData = [];
    console.assert(sortedTestData.length === sortedCaseData.length);
    for(let i=0; i<sortedTestData.length; i++) {
      console.assert(sortedTestData[i].datum === sortedCaseData[i].datum);
      let tauxPos = 0.;
      if(sortedTestData[i].entries > 0) {
        tauxPos = 100.0*sortedCaseData[i].entries/sortedTestData[i].entries;
      }
      let newObj = {
        'datum' : sortedTestData[i].datum,
        'pop' : sortedTestData[i].pop,
        'entries' : tauxPos };
      preprocessedData.push(newObj);
    }
    preprocessedData.pop();
  } else if(dataType === 'tests') {
    preprocessedData = mergeTestsDatasets(region);
    preprocessedData.pop();
  } else {
    let regionData = defineCasePerRegion(region, COVID_DATA_DICT[dataType]);
    let sortedData = sortDataByDate(regionData);
    preprocessedData = sortedData;
    preprocessedData.pop(); // Remove todays incomplete data
  }

  let predictedData = estimateFittedData(preprocessedData);

  for(let i=0; i<predictedData.fitted.length; i++) {
    preprocessedData[i].fitted = predictedData.fitted[i] >= 0. ? predictedData.fitted[i].toFixed(2) : 0.;
    preprocessedData[i].fitted = parseFloat(preprocessedData[i].fitted);

    preprocessedData[i].y = preprocessedData[i].entries.toFixed(2);
    preprocessedData[i].yFitted = parseFloat(preprocessedData[i].fitted).toFixed(2);
    preprocessedData[i].yPer100K = (preprocessedData[i].entries*100000/preprocessedData[i].pop).toFixed(2);
    preprocessedData[i].yFittedPer100K = (preprocessedData[i].fitted*100000/preprocessedData[i].pop).toFixed(2);

    preprocessedData[i].y = parseFloat(preprocessedData[i].y);
    preprocessedData[i].yFitted = parseFloat(preprocessedData[i].yFitted);
    preprocessedData[i].yPer100K = parseFloat(preprocessedData[i].yPer100K);
    preprocessedData[i].yFittedPer100K = parseFloat(preprocessedData[i].yFittedPer100K);


  }
  return preprocessedData;
}

const defineSummarizedData = function(
  region = 'CH',
  numberOfDays = 7) {

  let summarizedData = {};
  for(let key of DICT_KEYS) {
    let data = defineDataForCombinedChart(region, key);
    summarizedData[key] = {};
    let lastX = data.slice(Math.max(0, data.length - numberOfDays));
    summarizedData[key].nDays = numberOfDays;
    summarizedData[key].totalEntries = (lastX.reduce((x,myElem) => (x + myElem.entries), 0)).toFixed(2);
    summarizedData[key].totalEntriesPer100K = (summarizedData[key].totalEntries*100000/lastX[0].pop).toFixed(2);
    summarizedData[key].avgEntries = (summarizedData[key].totalEntries/summarizedData[key].nDays).toFixed(2);
    summarizedData[key].avgEntriesPer100K = (summarizedData[key].totalEntriesPer100K/summarizedData[key].nDays).toFixed(2);
  }
  return summarizedData;
}

const cleanAttributes = function(entries, attrsList = TO_KEEP_ATTRIBUTES) {
  for(let entry of entries) {
    Object.keys(entry).forEach( key => {
      if(!attrsList.includes(key)) {
        delete entry[key];
      }
    })
  }
}

const preProcessCasesData = function() {

  // Data by regions and type
  const regions = defineGeoRegions();
  for(let region of regions) {
    for(let dataType of DICT_KEYS) {
      let data = defineDataForCombinedChart(region, dataType);
      let stringifiedData = JSON.stringify(data);
      let dataCopy = JSON.parse(stringifiedData);
      cleanAttributes(dataCopy);
      let filename = OUTPUT_DATA_DIR + ([dataType, region]).join('_') + '.json';
      console.log(filename);
      fs.writeFile(filename, JSON.stringify(dataCopy, null, 4), (err) => {
        if (err) throw err;
        else console.log("Written : " + filename);
      });
    }
  }

  // Summarized data by regions, type and number of days
  for(let region of regions) {
    let overallSummaryPerRegion = {};
    for(let nDays of NB_OF_DAYS) {
      let data = defineSummarizedData(region, nDays);
      overallSummaryPerRegion['NB_DAYS_'+nDays] = data;
    }
    let stringifiedData = JSON.stringify(overallSummaryPerRegion, null, 4);
    let filename = OUTPUT_DATA_DIR + 'Summary_' + region + '.json';
    fs.writeFile(filename, stringifiedData, (err) => {
      if (err) throw err;
      else console.log("Written : " + filename);
    });
    fs.writeFileSync(filename, stringifiedData);
  }
};

module.exports = {preProcessCasesData};