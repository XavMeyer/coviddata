#!/bin/bash

set -e

clean_up () {
    ARG=$?
    echo "> clean_up"

    rm -f overview*
    rm -f sources-json.zip*
    rm -f master.zip*
    rm -rf last_data*
    rm -rf re_data*
    rm out.txt*

    exit $ARG
}
trap clean_up EXIT

uname_out="$(uname -s)"
case "${uname_out}" in
    Linux*)     machine=Linux; compiler=g++;;
    Darwin*)    machine=Mac; compiler=clang++;;
    CYGWIN*)    machine=Windows-cygwin; toolchain="x86_64-w64-mingw32"; compiler="${toolchain}-g++";;
    *)          machine="UNKNOWN"
esac

rm -rf last_data
wget https://www.covid19.admin.ch/fr/overview

if [ "${machine}" == "Mac" ]; then
  day_data_url=`gsed -rn 's|.*/api/data/([[:digit:]]+[[:punct:]][[:alnum:]]+)/downloads/sources-json.zip.*|\1|p' overview | head -n 1`
else
  day_data_url=`sed -rn 's|.*/api/data/([[:digit:]]+[[:punct:]][[:alnum:]]+)/downloads/sources-json.zip.*|\1|p' overview | head -n 1`
fi
echo "${day_data_url}"
wget https://www.covid19.admin.ch/api/data/${day_data_url}/downloads/sources-json.zip
mkdir -p last_data
mkdir -p last_data/processed
unzip sources-json.zip -d last_data


rm -rf re_data
wget https://github.com/covid-19-Re/dailyRe-Data/archive/master.zip
mkdir -p re_data/
mkdir -p re_data/processed
unzip master.zip -d re_data

# preprocessing
echo "Preprocessing starting"
node -v
node preprocessors/preProcess.js > out.txt

# copy preprocessed files
echo "Preprocessing Done - Copying to website data"
mkdir -p ../CovidCH/src/case_data/
cp last_data/processed/*.json ../CovidCH/src/case_data/.
mkdir -p ../CovidCH/src/re_data/
cp re_data/processed/*.json ../CovidCH/src/re_data/.
# Small fix
echo "Fixing a name"
mv ../CovidCH/src/re_data/CHE_CHE.json ../CovidCH/src/re_data/CHE_CH.json

