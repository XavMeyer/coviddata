# Where to get all the data!

## Stringency

### Oxford research:

* [Link to website](https://www.bsg.ox.ac.uk/research/research-projects/coronavirus-government-response-tracker)
* [Link to github](https://github.com/OxCGRT/covid-policy-tracker)

Example of API query:
> https://covidtrackerapi.bsg.ox.ac.uk/api/v2/stringency/date-range/2020-12-01/2020-12-15
